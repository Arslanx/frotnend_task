import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ListingComponent from "./Components/ListingComponent/ListingComponent";
import TaskForm from "./Components/TaskForm/TaskForm";
import BulkDelete from "./Components/BulkDelete/BulkDelete";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={ListingComponent} />
        <Route exact path="/create" component={TaskForm} />
        <Route exact path="/delete" component={BulkDelete} />
      </Switch>
    </Router>
    // <Route path="/about" component={About} /> */
    // <Navbar />
  );
}

export default App;
