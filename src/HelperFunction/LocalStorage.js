export const setObjInLS = (key, value) => {
  localStorage.setItem(key, value);
};

export const getObjFromLS = (key) => {
  return JSON.parse(localStorage.getItem(key));
};

export const clearLS = (key) => {
  localStorage.clear();
};
