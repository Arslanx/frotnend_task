import React, { Fragment, useState, useEffect } from "react";
import { getObjFromLS, setObjInLS } from "../../HelperFunction/LocalStorage";
import { useHistory } from "react-router-dom";
import Header from "../Header/Header";
import "./BulkDelete.css";

export default function BulkDelete() {
  const history = useHistory();
  let listing = getObjFromLS("taskList");
  let [selected, setSelected] = useState([]);
  useEffect(() => {
    console.log(selected);
  }, [selected, listing]);
  function deleteSelected() {
    listing = listing.filter((val) => !selected.includes(val));
    setObjInLS("taskList", JSON.stringify(listing));
    history.push("/delete");
  }
  return (
    <Fragment>
      <Header />
      <div className="bulk_listing">
        <button disabled={selected.length === 0} onClick={deleteSelected}>
          Delete Slected
        </button>
        {listing &&
          listing.map((el, i) => (
            <div key={el}>
              <input
                type="checkbox"
                onChange={(e) =>
                  e.target.checked === true
                    ? setSelected((prev) => [...prev, el])
                    : setSelected(
                        selected.filter((unselect) => unselect !== el)
                      )
                }
              />
              <span>{el}</span>
            </div>
          ))}
      </div>
    </Fragment>
  );
}
