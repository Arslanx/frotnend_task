import React from "react";
import { useHistory } from "react-router-dom";
import { clearLS } from "../../HelperFunction/LocalStorage";
import "./Header.css";

export default function Header() {
  const history = useHistory();
  function deleteAll() {
    clearLS();
    history.push("/create");
  }
  return (
    <div>
      <div className="top-header">
        <button className="header-btn" onClick={() => history.push("/create")}>
          Create Task
        </button>
        <button className="header-btn" onClick={() => history.push("/")}>
          Listing
        </button>
        <button className="header-btn" onClick={() => history.push("/delete")}>
          Bulk Delete Task
        </button>
        {window.location.pathname.includes("delete") && (
          <button className="header-btn" onClick={() => deleteAll()}>
            Delete All
          </button>
        )}
      </div>
    </div>
  );
}
