import React, { Fragment } from "react";
import { getObjFromLS } from "../../HelperFunction/LocalStorage";
import Header from "../Header/Header";
import "./ListingComponent.css";

export default function ListingComponent() {
  let listing = getObjFromLS("taskList");
  return (
    <Fragment>
      <Header />
      {listing.length > 0 ? (
        <div className="listing">
          {listing && listing.map((el) => <li key={el}>{el}</li>)}
        </div>
      ) : (
        <div style={{ display: "flex", justifyContent: "center" }}>
          Nothing to Display
        </div>
      )}
    </Fragment>
  );
}
