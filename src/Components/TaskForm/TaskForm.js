import React, { Fragment, useState, useEffect } from "react";
import { getObjFromLS, setObjInLS } from "../../HelperFunction/LocalStorage";
import Header from "../Header/Header";
import { useHistory } from "react-router-dom";
import "./TaskForm.css";

export default function TaskForm() {
  const history = useHistory();
  let Submitted = [];
  const [value, setValue] = useState("");
  function handleFormSubmit(e) {
    e.preventDefault();
    e.stopPropagation();
    let dataFromLS = getObjFromLS("taskList");
    if (dataFromLS) {
      dataFromLS.push(value);
      setObjInLS("taskList", JSON.stringify(dataFromLS));
    } else {
      Submitted.push(value);
      setObjInLS("taskList", JSON.stringify(Submitted));
    }
    history.push("/");
  }
  return (
    <Fragment>
      <Header />
      <div>
        <form className="form-field" onSubmit={handleFormSubmit}>
          <label>Task Name</label>
          <input
            value={value}
            name="task"
            id="task"
            className="input-field"
            onChange={(e) => setValue(e.target.value)}
          />
          <button className="submit" type="submit">
            Submit
          </button>
        </form>
      </div>
    </Fragment>
  );
}
